<?php

call_user_func(static function () {
    $fields = [
        'api_mapping_state' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
    ];

    // Add new fields to pages:
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('tx_powermail_domain_model_mail', $fields);
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
        'tx_powermail_domain_model_mail',
        'api_mapping_state',
        '',
        ''
    );
});