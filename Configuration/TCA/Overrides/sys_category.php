<?php
defined('TYPO3_MODE') or die();


call_user_func(static function () {
    $fields = [
        'newsletter_segment_id' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:kdn_newsletter/Resources/Private/Language/locallang_db.xlf:sys_category.newsletter_segment_id',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['LLL:EXT:kdn_newsletter/Resources/Private/Language/locallang_db.xlf:sys_category.newsletter_segment_id.none', ''],
                ],
                'default' => 0,
                'itemsProcFunc' => \KDN\KdnNewsletter\Api\NewsletterItemProcHelper::class . '->addListGroupItems',
            ]
        ],
    ];

    // Add new fields to pages:
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('sys_category', $fields);
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
        'sys_category',
        'newsletter_segment_id',
        '',
        'after:description'
    );
});