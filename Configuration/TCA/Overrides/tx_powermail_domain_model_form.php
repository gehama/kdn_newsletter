<?php
defined('TYPO3_MODE') or die();


call_user_func(static function () {
    // Configure new fields:
    $fields = [
        'newsletter_subscription_type' => [
            'label' => 'LLL:EXT:kdn_newsletter/Resources/Private/Language/locallang_db.xlf:tx_powermail_domain_model_form.newsletter_subscription_type',
            'exclude' => 1,
            'onChange' => 'reload',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['LLL:EXT:kdn_newsletter/Resources/Private/Language/locallang_db.xlf:tx_powermail_domain_model_form.newsletter_subscription_type.0', 0],
                    ['LLL:EXT:kdn_newsletter/Resources/Private/Language/locallang_db.xlf:tx_powermail_domain_model_form.newsletter_subscription_type.1', 1],
                    ['LLL:EXT:kdn_newsletter/Resources/Private/Language/locallang_db.xlf:tx_powermail_domain_model_form.newsletter_subscription_type.2', 2],
                    ['LLL:EXT:kdn_newsletter/Resources/Private/Language/locallang_db.xlf:tx_powermail_domain_model_form.newsletter_subscription_type.3', 3],
                ],
                'default' => 0,
            ],
        ],
        'form_id' => [
            'exclude' => 1,
            'displayCond' => 'FIELD:newsletter_subscription_type:>:0',
            'label' => 'LLL:EXT:kdn_newsletter/Resources/Private/Language/locallang_db.xlf:tx_powermail_domain_model_form.form_id',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required'
            ],
        ],
        /*'newsletter_address_book' => [
            'exclude' => 1,
            'displayCond' => 'FIELD:newsletter_subscription_type:>:0',
            'label' => 'LLL:EXT:kdn_newsletter/Resources/Private/Language/locallang_db.xlf:tx_powermail_domain_model_form.newsletter_address_book',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['LLL:EXT:kdn_newsletter/Resources/Private/Language/locallang_db.xlf:tx_powermail_domain_model_form.newsletter_address_book.none', ''],
                ],
                'default' => 0,
                'itemsProcFunc' => \KDN\KdnNewsletter\Api\NewsletterItemProcHelper::class . '->addAddressBookItems',
            ]
        ],*/
    ];

    // Add new fields to pages:
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('tx_powermail_domain_model_form', $fields);
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
        'tx_powermail_domain_model_form',
        'newsletter_subscription_type, form_id',//, newsletter_address_book
        '',
        'after:css'
    );
});