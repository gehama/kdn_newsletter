<?php

$EM_CONF[$_EXTKEY] = [
	'title' => 'KDN Newsletter subscription',
	'description' => 'Newsletter subscription for KDN website',
	'category' => 'misc',
	'author' => 'Gert Hammes',
	'author_email' => 'info@gerthammes.de',
	'author_company' => '',
	'shy' => '',
	'priority' => '',
	'module' => '',
	'state' => 'stable',
	'internal' => '',
	'uploadfolder' => '0',
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 0,
	'lockType' => '',
	'version' => '1.1.0',
	'constraints' => [
		'depends' => [
            'typo3' => '9.5.0-10.4.99',
            'powermail' => '7.0.0-9.9.99',
            'tt_address' => '4.3.0-9.9.99',
        ],
		'conflicts' => [
        ],
		'suggests' => [
        ],
    ],
    'autoload' => [
        'psr-4' => [
            'KDN\\KdnNewsletter\\' => 'Classes'
        ],
    ],
];
