<?php
defined('TYPO3_MODE') || die ('Access denied.');

call_user_func(
    static function($extKey) {
        if (empty($GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['kdn_newsletter_group'])) {
            $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['kdn_newsletter_group'] = [];
        }
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'KDN.kdn_newsletter',
            'Subscription',
            [
                'Subscription' => 'edit',

            ],
            // non-cacheable actions
            [
                'Subscription' => 'edit',

            ]
        );
        /** @var \TYPO3\CMS\Extbase\SignalSlot\Dispatcher $signalSlotDispatcher */
        $signalSlotDispatcher = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(
            \TYPO3\CMS\Extbase\SignalSlot\Dispatcher::class
        );
        $signalSlotDispatcher->connect(
            \In2code\Powermail\Controller\FormController::class,
            'optinConfirmActionBeforeRenderView',
            \KDN\KdnNewsletter\Powermail\OptInConfirm::class,
            'confirmUser',
            FALSE
        );
        $signalSlotDispatcher->connect(
            \KDN\KdnEvents\Controller\EventController::class,
            'registerActionAfterDbSaved',
            \KDN\KdnNewsletter\KdnEvents\RegistrationPostProcessing::class,
            'postProcessRegistration',
            FALSE
        );
        $signalSlotDispatcher->connect(
            \In2code\Powermail\ViewHelpers\Misc\PrefillFieldViewHelper::class,
            'render',
            \KDN\KdnNewsletter\Powermail\PrefillNewsletterFields::class,
            'prefillFields',
            FALSE
        );
        $signalSlotDispatcher->connect(
            \In2code\Powermail\ViewHelpers\Misc\PrefillMultiFieldViewHelper::class,
            'render',
            \KDN\KdnNewsletter\Powermail\PrefillNewsletterFields::class,
            'prefillMultiFields',
            FALSE
        );
        $signalSlotDispatcher->connect(
            \In2code\Powermail\Controller\FormController::class,
            'formActionBeforeRenderView',
            \KDN\KdnNewsletter\Powermail\CheckFormAccess::class,
            'assertAccess',
            FALSE
        );
    },
    'kdn_newsletter'
);
