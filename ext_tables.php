<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function($extKey)
    {
        if (TYPO3_MODE == 'BE') {
            /**
             * Registers a Backend Module
             */
            \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
                'KDN.' . $extKey,
                'web',              // Make module a submodule of 'web'
                'kdnnewsletter',    // Submodule key
                '',                 // Position
                [
                    'Backend' => 'list, export',
                ],
                [
                    'access' => 'user,group',
                    'icon'   => 'EXT:kdn_newsletter/Resources/Public/Icons/module-kdnnewsletter.svg',
                    'labels' => 'LLL:EXT:kdn_newsletter/Resources/Private/Language/locallang_kdnnewsletter.xlf',
                ]
            );
        }
    },
    'kdn_newsletter'
);
