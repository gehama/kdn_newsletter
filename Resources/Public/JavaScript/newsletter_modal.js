var kdnNewsletterModal = {
    modal: null,
    buttonSubscribe: null,
    scriptId: 'n2g_script',
    isInBody: false,
    init: function () {
        var self = this;
        if (!document.getElementById(self.scriptId)) {
            if (self.initSubscribeButton()) {
                if (self.showOnce()) {
                    self.showModal();
                }
            }
            window.addEventListener('resize', self.debounce(function () {
                if (!self.isInBody && self.modal && !self.allowInContent()) {
                    self.hideModal();
                } else if (self.initSubscribeButton()) {
                    if (self.showOnce()) {
                        self.showModal();
                    }
                }
            }, 33));
        }
    },

    initSubscribeButton: function () {
        this.buttonSubscribe = document.getElementById('newsletter-subscribe');
        if (this.buttonSubscribe) {
            if (this.buttonSubscribe.classList.contains('initialized')) {
                return this.isVisible(this.buttonSubscribe);
            }
            this.buttonSubscribe.removeAttribute('style');
            var parentElt = document.querySelector(this.buttonSubscribe.dataset.append);
            if (parentElt && this.isVisible(this.buttonSubscribe)) {
                var navItem;
                if (parentElt.classList.contains('nav')) {
                    navItem = document.createElement('li');
                    navItem.classList.add('nav-item');
                    navItem.classList.add('nav-item-newsletter');
                    navItem.innerHTML = '<ul class="first"><li class="first"></li></ul>';
                    navItem.querySelector('li.first').appendChild(this.buttonSubscribe);
                } else {
                    parentElt.innerHTML = '';
                    navItem = document.createElement('div');
                    navItem.classList.add('nav-item-newsletter');
                    navItem.appendChild(this.buttonSubscribe);
                }
                parentElt.appendChild(navItem);
                this.buttonSubscribe.classList.add('initialized');
                this.initButtonEvents();
                return true;
            }
        }
        return false;
    },
    initButtonEvents: function () {
        var self = this;
        this.buttonSubscribe.addEventListener('click', function (evt) {
            evt.preventDefault();
            if (!self.isInBody) {
                var contentElt = document.getElementById('content');
                contentElt.classList.remove('has-modal');
                self.isInBody = true;
                self.showModal();
                var newParent = document.getElementById('footer');
                if (newParent) {
                    newParent = newParent.querySelector('.footer-meta');
                }
                if (!newParent) {
                    newParent = document.querySelector('body');
                }
                newParent.appendChild(self.modal);
            } else {
                self.toggleModal();
            }
            window.scrollTo(0,document.body.scrollHeight);
        }, false);
    },
    allowInContent: function () {
        const vw = Math.max(document.documentElement.clientWidth || 0, window.innerWidth || 0);
        return vw > 1199;
    },

    showOnce: function () {
        if (this.allowInContent()) {
            if (window.location.hash === '#modal-newsletter-subscribe') {
                return true;
            }
            if (this.cookie.get('kdnNewsletterShown') !== '1') {
                this.cookie.set('kdnNewsletterShown', '1', 365);
                return true;
            }
        }
        return false;
    },
    cookie: {
        set: function (cname, cvalue, exdays) {
            var d = new Date();
            d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
            var expires = "expires=" + d.toUTCString();
            document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
        },
        get: function (cname) {
            var name = cname + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) === ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) === 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        },
    },

    toggleModal: function () {
        if (!this.modal || this.modal.classList.contains('fade')) {
            this.showModal();
        } else {
            this.hideModal();
        }
        return true;
    },

    hideModal: function () {
        if (this.modal) {
            this.modal.classList.add('fade');
        }
    },

    showModal: function () {
        this.createModal();
        if (!this.modal.classList.contains('show')) {
            this.modal.classList.add('show');
            this.modal.classList.remove('fade');
            document.querySelector('body').classList.add('has-inline-modal');
            this.initializeModalContent();
        }
    },
    initializeModalContent: function () {
        var self = this;
        if (!this.modal.classList.contains('loaded')) {
            this.modal.classList.add('loaded');
            var container = this.modal.querySelector('.modal-body');
            var privacyHtml = '';
            var observer = new MutationObserver(function (mutations) {
                for (let mutation of mutations) {
                    if (mutation.type === 'childList') {
                        for (var i = 0, n = mutation.addedNodes.length; i < n; i++) {
                            var elt = mutation.addedNodes[i];
                            if (elt.classList.contains('form-row')) {
                                if (elt.querySelector('.required')
                                    || elt.textContent.indexOf('Nachname') >= 0) {
                                    elt.classList.add('show');
                                    var labelElt = elt.querySelector('label');
                                    if (labelElt) {
                                        var inputElt = elt.querySelector('input');
                                        if (!inputElt || inputElt.getAttribute('type') !== "checkbox") {
                                            labelElt.parentNode.classList.add('col-label');
                                        }
                                        var selectElt = elt.querySelector('select');
                                        if (selectElt) {
                                            selectElt.querySelectorAll('option')[0].textContent = labelElt.textContent;
                                            selectElt.selectedIndex = 0;
                                        }
                                    }
                                } else {
                                    var btn = elt.querySelector('.btn');
                                    if (btn) {
                                        elt.classList.add('show');
                                        elt.classList.add('form-actions');
                                        btn.textContent = self.buttonSubscribe.querySelector('.label').textContent;
                                    } else if (elt.textContent.indexOf('Datenschutzbestimmungen') > 0) {
                                        privacyHtml = elt.innerHTML;
                                    }
                                }
                            }
                        }
                        if (container.querySelector('.btn-primary')) {
                            observer.disconnect();
                        }
                    }
                }
            });

            observer.observe(container, {childList: true, subtree: true});
            this.modal.addEventListener('transitionend', function (evt) {
                if (evt.target.classList.contains('fade')) {
                    evt.target.classList.remove('show');
                    document.querySelector('body').classList.remove('has-inline-modal');
                }
            }, false);
        }
    },

    createModal: function () {
        var self = this;
        if (!this.modal) {
            var scriptSource = '!function(e,t,n,c,r,a,i){e.Newsletter2GoTrackingObject=r,e[r]=e[r]||function(){(e[r].q=e[r].q||[]).push(arguments)},e[r].l=1*new Date,a=t.createElement(n),i=t.getElementsByTagName(n)[0],a.async=1,a.src=c,i.parentNode.insertBefore(a,i)}(window,document,"script","https://static.newsletter2go.com/utils.js","n2g");var config = {"container": {"type": "div","class": "form-horizontal form-newsletter","style": ""},"row": {"type": "div","class": "form-row form-group","style": ""},"columnLeft": {"type": "div","class": "col-12 col-md-2","style": ""},"columnRight": {"type": "div","class": "col-12 col-md-10","style": ""},"label": {"type": "label","class": "","style": ""},"input": {"class": "form-control","style": ""},"checkbox": {"type": "input", "class": "form-check-input","style": ""},"dropdown": {"type": "select","class": "form-control","style": ""},"button": {"type": "button","class": "btn btn-primary","style": ""}};n2g(\'create\', \'' + this.buttonSubscribe.dataset.projectId + '\');n2g(\'subscribe:createForm\', config);';
            var html = '<div class="modal-dialog">' +
                '    <div class="modal-content">' +
                '      <div class="modal-header b-blau">\n' +
                '        <span class="modal-icon"><span class="oi oi-envelope-closed"></span></span>' +
                '        <h5 class="modal-title" >News</h5>\n' +
                '        <button type="button" class="close" data-dismiss="modal" aria-label="Close">\n' +
                '          <span aria-hidden="true">×</span>\n' +
                '        </button>\n' +
                '      </div>' +
                '       <div class="modal-body">' +
                '         <p class="modal-body-title">Alles auf einen Blick</p>' +
                '      </div>' +
                '    </div>' +
                '  </div>';
            this.modal = document.createElement('div');
            this.modal.setAttribute('id', 'modal-newsletter-subscribe');
            this.modal.classList.add('modal-aside');
            this.modal.classList.add('fade');
            this.modal.classList.add('modal-newsletter-subscribe');
            this.modal.innerHTML = html;

            var scriptElem = document.createElement('script');
            scriptElem.setAttribute('id', this.scriptId);
            scriptElem.text = scriptSource;
            this.modal.querySelector('.modal-body').appendChild(scriptElem);
            var contentElt = document.getElementById('content');
            contentElt.appendChild(this.modal);
            contentElt.classList.add('has-modal');
            this.modal.querySelector('.close').addEventListener('click', function (evt) {
                evt.preventDefault();
                self.hideModal();
            }, false);
        }
        return true;
    },
    // Where el is the DOM element you'd like to test for visibility
    isVisible: function (el) {
        return (el.offsetParent !== null)
    },
    debounce: function (func, wait, immediate) {
        var timeout;
        return function () {
            var context = this, args = arguments;
            var later = function () {
                timeout = null;
                if (!immediate) func.apply(context, args);
            };
            var callNow = immediate && !timeout;
            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
            if (callNow) func.apply(context, args);
        };
    }
};

if (
    document.readyState === "complete" ||
    (document.readyState !== "loading" && !document.documentElement.doScroll)
) {
    kdnNewsletterModal.init();
} else {
    document.addEventListener("DOMContentLoaded", function () {
        kdnNewsletterModal.init();
    });
}