#
# Table structure for table 'tx_powermail_domain_model_form'
#
CREATE TABLE tx_powermail_domain_model_form (
	newsletter_subscription_type tinyint(4) unsigned DEFAULT '0' NOT NULL,
	form_id varchar(100) DEFAULT '' NOT NULL,
	newsletter_address_book varchar(100) DEFAULT '' NOT NULL,
);

#
# Table structure for table 'sys_category'
#
CREATE TABLE sys_category (
	newsletter_segment_id varchar(100) DEFAULT '' NOT NULL,
);


#
# Table structure for table 'tx_powermail_domain_model_mail'
#
CREATE TABLE tx_powermail_domain_model_mail (
	api_mapping_state tinyint(4) unsigned DEFAULT '0' NOT NULL,
	KEY api_mapping_state (api_mapping_state)
);