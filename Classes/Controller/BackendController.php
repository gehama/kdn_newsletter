<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Gert Hammes
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace KDN\KdnNewsletter\Controller;

use KDN\KdnNewsletter\Service\AddressService;
use KDN\KdnNewsletter\Service\ExcelExportService;
use KDN\KdnNewsletter\Service\SpreadsheetExportService;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Exception\Page\PageNotFoundException;
use TYPO3\CMS\Core\TypoScript\TemplateService;
use TYPO3\CMS\Core\TypoScript\TypoScriptService;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\RootlineUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManager;
use TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings;
use TYPO3\CMS\Frontend\Page\PageRepository;

/**
 *
 *
 * @package kdn_addresss
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class BackendController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * TypoScript settings
     *
     * @var array
     */
    protected $settings = array();

    /**
     * id of selected page
     *
     * @var int
     */
    protected $id;

    /**
     * info of selected page
     *
     * @var array
     */
    protected $pageinfo;

    /**
     * Event repository
     *
     * @var \FriendsOfTYPO3\TtAddress\Domain\Repository\AddressRepository
     * @TYPO3\CMS\Extbase\Annotation\Inject
     */
    protected $addressRepository;

    protected function initializeAction()
    {
        /** @var AddressService $addressService */
        $addressService = $this->objectManager->get(AddressService::class);
        $pagesWithRegistrations = $addressService->getAddressPagesInfo();
        $pageIds = array_keys($pagesWithRegistrations);

        /** @var \TYPO3\CMS\Core\Authentication\BackendUserAuthentication $beUser */
        $beUser = $GLOBALS['BE_USER'];

        // Only load address and registrations from pages the current user has access to
        $allowedPageIds = array();
        $selectedPageId = 0;
        foreach ($pageIds as $pageId) {
            $pageInfo = BackendUtility::readPageAccess($pageId, $beUser->getPagePermsClause(1));
            if (!empty($pageInfo)) {
                if (empty($selectedPageId)) {
                    $selectedPageId = $pageId;
                    $this->pageinfo = $pageInfo;
                }
                $allowedPageIds[] = $pageId;
            }
        }
        $this->id = $selectedPageId > 0 ? $selectedPageId : (int)GeneralUtility::_GP('id');
        if (empty($allowedPageIds)) {
            $allowedPageIds[] = $this->id;
        }
        /** @var Typo3QuerySettings $defaultQuerySettings */
        $defaultQuerySettings = $this->objectManager->get(Typo3QuerySettings::class);
        $defaultQuerySettings->setStoragePageIds($allowedPageIds);
        $defaultQuerySettings->setRespectStoragePage(true);
        // don't add fields from enable fields constraint
        //$defaultQuerySettings->setRespectEnableFields(FALSE);
        // don't add sys_language_uid constraint
        //$defaultQuerySettings->setRespectSysLanguage(FALSE);
        $this->addressRepository->setDefaultQuerySettings($defaultQuerySettings);

        $this->settings = $this->configurationManager->getConfiguration(
            ConfigurationManager::CONFIGURATION_TYPE_SETTINGS
        );
        if (empty($this->settings)) {
            $this->settings = $this->initSettingsForPageId($this->id);
        }
        parent::initializeAction();
    }

    /**
     * Fallback function to load TypoScript settings for module if page outside of main page tree (containing
     * the address registration pages) is selected;
     * Instead of loading the TypoScript from the current page, the address storage page is given
     *
     * @param int $pageId
     * @return array|null
     */
    private function initSettingsForPageId($pageId)
    {

        $settings = null;
        if ($pageId > 0) {
            $this->id = $pageId;

            /** @var TemplateService $template */
            $template = GeneralUtility::makeInstance(TemplateService::class);
            // do not log time-performance information
            $template->tt_track = 0;
            $template->init();
            // Get the root line
            /** @var PageRepository $sysPage */
            $sysPage = GeneralUtility::makeInstance(PageRepository::class);
            // get the rootline for the current page
            try {
                /** @var RootlineUtility $rootLineUtility */
                $rootLineUtility = GeneralUtility::makeInstance(RootlineUtility::class, $pageId);
                $rootLine = $rootLineUtility->get();
            } catch (PageNotFoundException $e) {
                $rootLine = [];
            }
            // This generates the constants/config + hierarchy info for the template.
            $template->runThroughTemplates($rootLine, 0);
            $template->generateConfig();
            if (isset($template->setup['module.']['tx_kdnnewsletter.']['settings.'])) {
                /** @var TypoScriptService $tsService */
                $tsService = GeneralUtility::makeInstance(TypoScriptService::class);
                $settings = $tsService->convertTypoScriptArrayToPlainArray($template->setup['module.']['tx_kdnaddresss.']['settings.']);
            }
        }
        return $settings;
    }

    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {
        /** @var AddressService $addressService */
        $addressService = $this->objectManager->get(AddressService::class);
        $addresses = $this->addressRepository->findAll();
        $this->view->assignMultiple([
            'addresses' => $addresses,
        ]);
    }

    /**
     * action export
     *
     * @return void
     */
    public function exportAction()
    {
        $addresses = $this->addressRepository->findAll();
        if (class_exists('\PhpOffice\PhpSpreadsheet\Spreadsheet')) {
            $exportService = $this->objectManager->get(SpreadsheetExportService::class);
        } else {
            $exportService = $this->objectManager->get(ExcelExportService::class);
        }
        /** @var \KDN\KdnNewsletter\Service\AbstractExportService $exportService */


        $sheetName = 'Anmeldungen';//LocalizationUtility::translate('heading.addresses', 'kdn_addresss');
        $fileBaseName = 'Anmeldungen_Informationsverteiler';//$sheetName . '_' . ($address !== null ? $address->getTitle() : 'KDN_Event');
        $exportService->create($sheetName, $addresses);
        $exportService->sendFile($fileBaseName, 'xlsx');
    }
}
