<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2021 Gert Hammes
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace KDN\KdnNewsletter\Controller;

use KDN\KdnNewsletter\Api\Newsletter2Go;
use KDN\KdnNewsletter\Service\FrontendSessionHandlerService;
use KDN\KdnNewsletter\Service\MailService;
use KDN\KdnNewsletter\Utility\ConfigurationUtility;
use TYPO3\CMS\Core\Messaging\FlashMessage;
use TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

/**
 *
 *
 * @package kdn_events
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class SubscriptionController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * action registration edit
     *
     * @param string $hash
     * @param string $email
     * @param string $notes Used as Honeypot field for spammers
     *
     * @return void
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\StopActionException
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\UnsupportedRequestTypeException
     */
    public function editAction($hash = null, $email = null, $notes = null)
    {
        $newsletterSettings = ConfigurationUtility::getAuthData();
        if (!empty($newsletterSettings)) {
            $formStep = 0;
            $filteredEmail = trim(strip_tags($email));
            $isValidEmail = !empty($filteredEmail) && filter_var($filteredEmail, FILTER_VALIDATE_EMAIL);
            // Email submitted, but invalid
            if (null !== $email && !$isValidEmail) {
                $message = LocalizationUtility::translate('tx_kdnnewsletter_profile.form_error_email', 'kdn_newsletter');
                $this->addFlashMessage($message, '', FlashMessage::ERROR);
                $this->redirect('edit');
            }
            $apiService = Newsletter2Go::createInstance($newsletterSettings);
            $cleanedHash = trim(strip_tags($hash));
            if (!empty($cleanedHash)) {
                $result = $apiService->getRecipient($hash);
                if ($result && (int)$result->status === 200) {
                    //$recipientModel = $result->value[0];
                    $targetPage = (int)$this->settings['profileEditPage'];
                    /** @var FrontendSessionHandlerService $feSessionHandler */
                    $feSessionHandler = $this->objectManager->get(FrontendSessionHandlerService::class);
                    $feSessionHandler->store('profileEditHash', $hash);
                    /** @var UriBuilder $uriBuilder */
                    $uriBuilder = $this->objectManager->get(UriBuilder::class);
                    $uri = $uriBuilder->reset()->setTargetPageUid($targetPage)->setCreateAbsoluteUri(true);
                    $redirectUrl = $uri->build();
                    $this->redirectToUri($redirectUrl);
                    $formStep = 4;
                } else {
                    $message = LocalizationUtility::translate('tx_kdnnewsletter_profile.form_error_hash', 'kdn_newsletter');
                    $this->addFlashMessage($message, '', FlashMessage::ERROR);
                    $this->redirect('edit');
                }
            } elseif ($isValidEmail) {
                // Always show request confirmation
                $formStep = 1;
                $recipientModel = $apiService->getRecipientDetailsByEmail($filteredEmail);
                if (null !== $recipientModel) {
                    /** @var MailService $mailService */
                    $mailService = $this->objectManager->get(MailService::class);
                    $mailService->sendProfileEditRequestEmail($filteredEmail, $recipientModel->id, $this->settings);
                }
            }
        } else {
            $formStep = 99;
        }

        $this->view->assignMultiple(array(
            'notes' => (string)$notes,
            'formStep' => $formStep,
            'contentObject' => $this->configurationManager->getContentObject(),
            'settings' => $this->settings,
        ));
    }

    /*
     * Deactivate flash message for form errors
     *
     * @see \TYPO3\CMS\Extbase\MVC\Controller\ActionController::getErrorFlashMessage()
     */
    protected function getErrorFlashMessage()
    {
        return FALSE;
    }

}
