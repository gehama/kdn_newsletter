<?php

namespace KDN\KdnNewsletter\Powermail;

use In2code\Powermail\Controller\FormController;
use In2code\Powermail\Domain\Model\Mail;
use In2code\Powermail\Domain\Repository\MailRepository;
use In2code\Powermail\Utility\HashUtility;
use KDN\KdnNewsletter\Service\ApiHelper;
use KDN\KdnNewsletter\Utility\ConfigurationUtility;
use KDN\KdnNewsletter\Utility\DbUtility;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\DataHandling\DataHandler;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\HttpUtility;
use TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder;
use TYPO3\CMS\Extbase\Object\ObjectManager;

class OptInConfirm
{

    /**
     * @var MailRepository
     */
    protected $mailRepository;

    /**
     * @param MailRepository $mailRepository
     * @return void
     */
    public function injectMailRepository(MailRepository $mailRepository)
    {
        $this->mailRepository = $mailRepository;
    }

    /**
     * Enable frontend user on opt in if found
     *
     * @param int $mail mail uid
     * @param string $hash Given Hash String
     * @param FormController $originalService
     * @throws \Exception
     */
    public function confirmUser($mail, $hash, FormController $originalService)
    {
        /** @var \In2code\Powermail\Domain\Model\Mail|null $mailModel */
        $mailModel = $this->mailRepository->findByUid($mail);
        /** @noinspection PhpUnhandledExceptionInspection */
        if ($mailModel !== null && HashUtility::isHashValid($hash, $mailModel)) {
            $table = (string)ConfigurationUtility::getExtConfValue('recipientTable');
            $enableTableStorage = (bool)ConfigurationUtility::getExtConfValue('enableTableStorage');
            if ($enableTableStorage && !empty($table)) {
                //$wasAlreadyEnabled = !$mailModel->getHidden();
                //$userEmail = $mailModel->getSenderMail();
                $formValues = [];
                foreach ($mailModel->getAnswers() as $answer) {
                    $formValues[$answer->getField()->getMarker()] = $answer->getValue();
                }
                if (!empty($formValues['optin_email'])) {
                    $this->optInNewsletter($mailModel, $formValues);
                } elseif (!empty($formValues['optout_email'])) {
                    $this->optOutNewsletter($mailModel, $formValues, $table);
                }
            }
        }
    }

    private function redirect($pageId)
    {
        /** @var ObjectManager $objectManager */
        $objectManager = GeneralUtility::makeInstance(ObjectManager::class);
        /** @var UriBuilder $uriBuilder */
        $uriBuilder = $objectManager->get(UriBuilder::class);
        $uri = $uriBuilder->reset()->setTargetPageUid($pageId)->setCreateAbsoluteUri(true);
        $redirectUrl = $uri->build();
        if (!empty($redirectUrl)) {
            HttpUtility::redirect($redirectUrl);
        }
    }

    private function optInNewsletter(Mail $mail, $formValues)
    {
        $fieldMap = [
            'optin_email' => 'email',
            'gender' => 'gender',
            //'titel' => 'title',
            'first_name' => 'first_name',
            'last_name' => 'last_name',
            'company' => 'company',
//                'address' => 'address',
//                'zip' => 'zip',
//                'city' => 'city',
//                'telephone' => 'telephone',
//                'fax' => 'fax',
        ];
        $mappedFormValues = [];
        foreach ($fieldMap as $sourceField => $targetField) {
            if (isset($formValues[$sourceField])) {
                $mappedFormValues[$targetField] = trim(strip_tags($formValues[$sourceField]));
            }
        }
        if (isset($formValues['categories'])) {
            $mappedFormValues['categories'] = $formValues['categories'];
        }
        $dbRecord = self::saveRecipient($mappedFormValues);
        if (!empty($dbRecord['uid'])) {
            $connection = DbUtility::getConnection('tx_powermail_domain_model_mail');
            $connection->update(
                'tx_powermail_domain_model_mail',
                ['hidden' => 0],
                ['uid' => (int)$mail->getUid()]
            );
        }
        $disableOptInMails = (bool)ConfigurationUtility::getExtConfValue('disableOptInMails');
        // Redirect to prevent emails from being sent
        if ($disableOptInMails) {
            $redirectPid = (int)ConfigurationUtility::getExtConfValue('optInThankYouPid');
            $this->redirect($redirectPid);
        }

    }

    /**
     * Save the recipient data
     * @param array $formValues
     * @return array
     */
    public static function saveRecipient($formValues): array
    {
        if (!empty($formValues['categories'])) {
            $processedCategoryIdList = [];
            foreach ($formValues['categories'] as $value) {
                if (!is_numeric($value)) {
                    $where = 'title = \'' . trim(strip_tags($value)) . '\'';
                    $dbCategoryRecord = DbUtility::fetchRow('uid', 'sys_category', $where);
                    if (!empty($dbCategoryRecord)) {
                        $processedCategoryIdList[] = (int)$dbCategoryRecord['uid'];
                    }
                } else {
                    $processedCategoryIdList[] = (int)$value;
                }
            }
            $formValues['categories'] = $processedCategoryIdList;
        }
        if (!empty($formValues['gender'])) {
            $formValues['gender'] = AnswerValueMapper::mapSalutationToGender($formValues['gender']);
        }
        $tableName = (string)ConfigurationUtility::getExtConfValue('recipientTable');
        $storagePid = (int)ConfigurationUtility::getExtConfValue('storagePid');
        $userEmail = $formValues['email'];
        $where = 'email = \'' . $userEmail . '\' AND pid = ' . $storagePid;
        $dbRecord = DbUtility::fetchRow('*', $tableName, $where);
        if (empty($dbRecord)) {
            $userData = [
                'hidden' => 0,
                'pid' => $storagePid,
            ];
            foreach ($formValues as $sourceField => $sourceValue) {
                if ((string)$sourceValue !== '') {
                    $userData[$sourceField] = $sourceValue;
                }
            }
            $dbRecord = self::saveTcaRow($tableName, $userData);
        } else {
            $userUpdateBind = [
                'hidden' => 0,
                'tstamp' => time(),
            ];
            foreach ($formValues as $sourceField => $sourceValue) {
                if (array_key_exists($sourceField, $dbRecord) && (string)$dbRecord[$sourceField] !== (string)$sourceValue) {
                    $userUpdateBind[$sourceField] = $sourceValue;
                }
            }
            if (!empty($dbRecord['uid'])) {
                $dbRecord = self::saveTcaRow($tableName, $userUpdateBind, (int)$dbRecord['uid']);
            }
        }
        return $dbRecord;
    }

    private function optOutNewsletter(Mail $mail, $formValues, $tableName)
    {
        $storagePid = (int)ConfigurationUtility::getExtConfValue('storagePid');
        $userEmail = $formValues['optout_email'];
        // categories
        $connection = DbUtility::getConnection($tableName);
        $where = 'email = \'' . $userEmail . '\' AND pid = ' . $storagePid;
        $dbRecord = DbUtility::fetchRow('*', $tableName, $where);
        $userUpdateBind = [
            'hidden' => 1,
            'tstamp' => time(),
        ];
        $disableOptOutMails = true;
        if (!empty($dbRecord['uid']) && !$dbRecord['hidden']) {
            $disableOptOutMails = false;

            $connection->update(
                'tx_powermail_domain_model_mail',
                ['hidden' => 0],
                ['uid' => (int)$mail->getUid()]
            );
            $connection->update(
                $tableName,
                $userUpdateBind,
                ['uid' => (int)$dbRecord['uid']]
            );
        }
        // Redirect to prevent emails from being sent
        if ($disableOptOutMails) {
            $redirectPid = (int)ConfigurationUtility::getExtConfValue('optOutThankYouPid');
            $this->redirect($redirectPid);
        }

    }

    /**
     * Creates a new TCA row for the given table with the given data
     * @param string $table
     * @param array $data
     * @param string|int $recordId The optional record id
     * @return array|null
     */
    private static function saveTcaRow($table, $data, $recordId = 'NEW_12346')
    {
        // make an instance of the DataHandler and create tt_address entry
        /** @var \TYPO3\CMS\Core\DataHandling\DataHandler $tce */
        $tce = GeneralUtility::makeInstance(DataHandler::class);
        $tmpId = $recordId;
        $altUserObject = ApiHelper::getBackendUser();
        $tce->start(
            [
                $table => [
                    $tmpId => $data,
                ]
            ],
            [],
            $altUserObject
        );
        $tce->process_datamap();

        $itemId = (int)(is_numeric($tmpId) ? $tmpId : $tce->substNEWwithIDs[$tmpId]);
        if (empty($itemId)) {
            unset($data['categories']);
            // TODO: insert categories without TCA
            $connection = DbUtility::getConnection($table);
            $data['crdate'] = time();
            $data['tstamp'] = time();
            $connection->insert($table, $data);
            $itemId = $connection->lastInsertId($table);
        }
        return BackendUtility::getRecord($table, $itemId);
    }
}