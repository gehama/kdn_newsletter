<?php

namespace KDN\KdnNewsletter\Powermail;

use In2code\Powermail\Controller\FormController;
use KDN\KdnNewsletter\Service\FrontendSessionHandlerService;
use TYPO3\CMS\Core\Exception\Page\PageNotFoundException;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\HttpUtility;
use TYPO3\CMS\Core\Utility\RootlineUtility;
use TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController;
use TYPO3\CMS\Frontend\Page\PageRepository;

class CheckFormAccess
{
    /**
     * @var FrontendSessionHandlerService
     */
    protected $frontendSessionHandlerService;

    /**
     * @param FrontendSessionHandlerService $frontendSessionHandlerService
     * @return void
     */
    public function injectFrontendSessionHandlerService(FrontendSessionHandlerService $frontendSessionHandlerService)
    {
        $this->frontendSessionHandlerService = $frontendSessionHandlerService;
    }

    /**
     * Redirect users if access to newsletter profile edit form was nt unlocked before accessing the form
     *
     * @param \In2code\Powermail\Domain\Model\Form|null $form The form
     * @param FormController $originalService
     * @throws \Exception
     */
    public function assertAccess($form, FormController $originalService)
    {
        if (null !== $form) {
            $formData = RecipientUpdater::getFormData($form);
            if (!empty($formData)) {
                //$this->formId = $formData['form_id'];
                $subscriptionType = (int)$formData['newsletter_subscription_type'];
                if ($subscriptionType === SaveRegistrationFinisher::SUBSCRIPTION_TYPE_SETTINGS
                    && !$this->frontendSessionHandlerService->get('profileEditHash')) {
                    $pageId = (int)$this->getTypoScriptFrontendController()->id;
                    try {
                        $rootLine = GeneralUtility::makeInstance(RootlineUtility::class, $pageId)->get();
                    } catch (PageNotFoundException $e) {
                        $rootLine = [];
                    }
                    if (!empty($rootLine)) {
                        foreach ($rootLine as $entry) {
                            if ((int)$entry['uid'] !== $pageId
                                && !$entry['hidden']
                                && ((int)$entry['doktype'] === PageRepository::DOKTYPE_DEFAULT)) {
                                $this->redirect($entry['uid']);
                            }
                        }
                    }
                    $redirectUrl = GeneralUtility::getIndpEnv('TYPO3_REQUEST_HOST');
                    HttpUtility::redirect($redirectUrl);
                }
            }
        }
    }

    /**
     * @return TypoScriptFrontendController
     * @SuppressWarnings(PHPMD.Superglobals)
     */
    protected function getTypoScriptFrontendController()
    {
        return $GLOBALS['TSFE'];
    }

    /**
     * Redirect to the given page id
     *
     * @param int $pageId
     */
    private function redirect(int $pageId): void
    {
        /** @var ObjectManager $objectManager */
        $objectManager = GeneralUtility::makeInstance(ObjectManager::class);
        /** @var UriBuilder $uriBuilder */
        $uriBuilder = $objectManager->get(UriBuilder::class);
        $uri = $uriBuilder->reset()->setTargetPageUid($pageId)->setCreateAbsoluteUri(true);
        $redirectUrl = $uri->build();
        if (!empty($redirectUrl)) {
            HttpUtility::redirect($redirectUrl);
        }
    }
}