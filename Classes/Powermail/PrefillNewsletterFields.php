<?php

namespace KDN\KdnNewsletter\Powermail;

use In2code\Powermail\Domain\Repository\MailRepository;
use KDN\KdnNewsletter\Api\Newsletter2Go;
use KDN\KdnNewsletter\Service\FrontendSessionHandlerService;
use KDN\KdnNewsletter\Utility\ConfigurationUtility;
use KDN\KdnNewsletter\Utility\DbUtility;

class PrefillNewsletterFields
{

    /**
     * @var MailRepository
     */
    protected $mailRepository;

    /**
     * @var FrontendSessionHandlerService
     */
    protected $frontendSessionHandlerService;

    /**
     * @var int
     */
    private $subscriptionType;

    /**
     * @param MailRepository $mailRepository
     * @return void
     */
    public function injectMailRepository(MailRepository $mailRepository)
    {
        $this->mailRepository = $mailRepository;
    }

    /**
     * @param FrontendSessionHandlerService $frontendSessionHandlerService
     * @return void
     */
    public function injectFrontendSessionHandlerService(FrontendSessionHandlerService $frontendSessionHandlerService)
    {
        $this->frontendSessionHandlerService = $frontendSessionHandlerService;
    }

    /**
     * Newsletter profile has been loaded
     * @var bool
     */
    protected $profileLoaded = false;

    /**
     * @var \stdClass|null
     */
    protected $profileData;

    /**
     * Pre-fill form fields for newsletter recipient
     *
     * @param \In2code\Powermail\Domain\Model\Field $field The current field
     * @param \In2code\Powermail\Domain\Model\Mail|null $mail The mail object
     * @param mixed $default Default value
     * @param \In2code\Powermail\ViewHelpers\Misc\PrefillFieldViewHelper $viewHelper The caller view helper
     * @throws \Exception
     */
    public function prefillFields($field, $mail, $default, $viewHelper)
    {
        if (null !== $recipientModel = $this->getNewsletterRecipientModel()) {
            if (null === $this->subscriptionType) {
                $formData = RecipientUpdater::getFormData($field);
                if (!empty($formData)) {
                    //$this->formId = $formData['form_id'];
                    $this->subscriptionType = (int)$formData['newsletter_subscription_type'];
                }
            }
            if ($this->subscriptionType === \KDN\KdnNewsletter\Powermail\SaveRegistrationFinisher::SUBSCRIPTION_TYPE_SETTINGS) {
                $processValue = $viewHelper->getValue();
                $marker = $field->getMarker();
                switch ($marker) {
                    case 'first_name':
                        $processValue = $recipientModel->first_name;
                        break;
                    case 'last_name':
                        $processValue = $recipientModel->last_name;
                        break;
                    case 'optin_email':
                    case 'email':
                        $processValue = $recipientModel->email;
                        break;
                }
                $viewHelper->setValue($processValue);
            }
        }
    }

    /**
     * Pre-fill form fields for newsletter recipient
     *
     * @param \In2code\Powermail\Domain\Model\Field $field The current field
     * @param \In2code\Powermail\Domain\Model\Mail|null $mail The mail object
     * @param int $cycle
     * @param mixed $default Default value
     * @param \In2code\Powermail\ViewHelpers\Misc\PrefillMultiFieldViewHelper $viewHelper The caller view helper
     * @throws \Exception
     */
    public function prefillMultiFields($field, $mail, int $cycle, $default, $viewHelper)
    {
        if (null !== $recipientModel = $this->getNewsletterRecipientModel()) {
            if (null === $this->subscriptionType) {
                $formData = RecipientUpdater::getFormData($field);
                if (!empty($formData)) {
                    //$this->formId = $formData['form_id'];
                    $this->subscriptionType = (int)$formData['newsletter_subscription_type'];
                }
            }
            if ($this->subscriptionType === \KDN\KdnNewsletter\Powermail\SaveRegistrationFinisher::SUBSCRIPTION_TYPE_SETTINGS) {
                $marker = $field->getMarker();
                $index = $cycle - 1;
                $options = $field->getModifiedSettings();
                $option = $options[$index] ?? null;
                switch ($marker) {
                    case 'categories':
                        if ($option && !empty($recipientModel->group_ids)) {
                            $where = 'title = \'' . trim(strip_tags($option['value'])) . '\' AND LENGTH(newsletter_segment_id) > 0';
                            $categoryData = DbUtility::fetchRow(['uid', 'title', 'newsletter_segment_id'], 'sys_category', $where);
                            if ($categoryData && in_array($categoryData['newsletter_segment_id'], $recipientModel->group_ids, false)) {
                                $viewHelper->setSelected(true);
                            }
                        }
                        break;
                    case 'gender':
                        if ($option && $option['value'] === $recipientModel->gender) {
                            $viewHelper->setSelected(true);
                        }
                        break;
                }
            }
        }
    }

    /**
     * Returns the recipient data or null of no data exist.
     * Loads the recipient data once
     *
     * @return \stdClass|null
     * @throws \Exception
     */
    protected function getNewsletterRecipientModel(): ?\stdClass
    {
        if (!$this->profileLoaded) {
            $this->profileLoaded = true;
            $profileEditHash = $this->frontendSessionHandlerService->get('profileEditHash');
            $newsletterSettings = ConfigurationUtility::getAuthData();
            if ($profileEditHash && $newsletterSettings) {
                $apiService = Newsletter2Go::createInstance($newsletterSettings);
                $result = $apiService->getRecipient($profileEditHash, false);
                if ($result && (int)$result->status === 200) {
                    $this->profileData = $result->value[0];
                }
            }
        }
        return $this->profileData;
    }
}