<?php
declare(strict_types=1);

namespace KDN\KdnNewsletter\Powermail;

use In2code\Powermail\Domain\Repository\MailRepository;
use In2code\Powermail\Finisher\AbstractFinisher;
use In2code\Powermail\Utility\ObjectUtility;
use KDN\KdnNewsletter\Api\Newsletter2Go;
use KDN\KdnNewsletter\Utility\ConfigurationUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;

/**
 * SendParametersFinisher to send params via CURL
 */
class SaveRegistrationFinisher extends AbstractFinisher
{
    private const SUBSCRIPTION_TYPE_SUBSCRIBE = 1;
    private const SUBSCRIPTION_TYPE_UNSUBSCRIBE = 2;
    public const SUBSCRIPTION_TYPE_SETTINGS = 3;

    /**
     * @var ConfigurationManagerInterface
     */
    protected $configurationManager;

    /**
     * @var array
     */
    protected $dataArray = [];

    /**
     * @var \TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer
     */
    protected $contentObject = null;

    /**
     * The api connection configuration
     *
     * @var array
     */
    protected $configuration;

    /**
     * @var int|null
     */
    private $subscriptionType;

    /**
     * @var string
     */
    private $formId;

    /**
     * @var MailRepository
     */
    protected $mailRepository;

    /**
     * @param MailRepository $mailRepository
     * @return void
     */
    public function injectMailRepository(MailRepository $mailRepository)
    {
        $this->mailRepository = $mailRepository;
    }

    /**
     * Send values via curl to a third party software
     *
     * @return void
     * @noinspection PhpUnused
     */
    public function sendFinisher()
    {
        if ($this->isEnabled()) {
            $data = $this->getDataArray();
            $this->contentObject->start($data);
            $apiService = Newsletter2Go::createInstance($this->configuration);
            $email = '';
            if (!empty($data['optin_email'])) {
                $email = $data['optin_email'];
            } elseif (!empty($data['optout_email'])) {
                $email = $data['optout_email'];
            } elseif (!empty($data['email'])) {
                $email = $data['email'];
            }
            if ($email) {
                $gender = '';
                if (!empty($data['gender'])) {
                    $gender = AnswerValueMapper::mapSalutationToGender($data['gender'], false);
                }
                $firstName = $data['first_name'] ?: '';
                $lastName = $data['last_name'] ?: '';
                $recipientData = $apiService->getRecipientDetailsByEmail($email, false);
                if (null !== $recipientData && $this->subscriptionType !== self::SUBSCRIPTION_TYPE_UNSUBSCRIBE) {
                    $categories = RecipientUpdater::getEnabledCategories($this->mail);
                    $groupList = $apiService->getGroupList();
                    $mappingState = empty($categories) ? 0 : 1;
                    RecipientUpdater::updateProfileData(
                        $this->mailRepository,
                        $this->mail,
                        $mappingState,
                        $groupList,
                        $apiService
                    );
                } elseif ($this->subscriptionType === self::SUBSCRIPTION_TYPE_SUBSCRIBE) {
                    $categories = RecipientUpdater::getEnabledCategories($this->mail);
                    $result = $apiService->addRecipientViaForm(
                        strtolower($this->formId),
                        $email,
                        $firstName,
                        $lastName,
                        $gender
                    );
                    $resultStatus = (int)$result->status;
                    // Mapping only is necessary if categories were selected
                    // Mark mail for further processing
                    if ($resultStatus > 300) {
                        $mappingState = empty($categories) ? 90 : 91;
                    } else {
                        $mappingState = empty($categories) ? 0 : 1;
                    }
                    RecipientUpdater::updateMappingState($this->mail->getUid(), $mappingState);

                } elseif ($this->subscriptionType === self::SUBSCRIPTION_TYPE_UNSUBSCRIBE) {
                    // The form ID must belong to an opt-out form! Otherwise this is the same as opt-in and only separated
                    // for clarity
                    $apiService->addRecipientViaForm(
                        strtolower($this->formId),
                        $email,
                        $firstName,
                        $lastName,
                        $gender
                    );
                }
            }
            $this->writeToDevelopmentLog();
        }
    }

    /**
     * Write devlog entry
     *
     * @return void
     */
    protected function writeToDevelopmentLog()
    {
        if ($this->configuration['debug']) {
            $logger = ObjectUtility::getLogger(__CLASS__);
            $logger->alert('SendPost Values', $this->getDataArray());
        }
    }

    /**
     * Add array to dataArray
     *
     * @param array $array
     * @return void
     */
    protected function addArrayToDataArray(array $array): void
    {
        $dataArray = $this->getDataArray();
        $dataArray = array_merge($dataArray, $array);
        $this->setDataArray($dataArray);
    }

    /**
     * @return array
     */
    public function getDataArray(): array
    {
        return $this->dataArray;
    }

    /**
     * @param array $dataArray
     * @return SaveRegistrationFinisher
     */
    public function setDataArray(array $dataArray): SaveRegistrationFinisher
    {
        $this->dataArray = $dataArray;
        return $this;
    }

    /**
     * Check if sendPost is activated
     *      - if form id and subscription type are set
     *      - if form was final submitted (without optin)
     *
     * @return bool
     */
    protected function isEnabled()
    {
        return $this->configuration && $this->isFormSubmitted() && $this->formId && $this->subscriptionType > 0;
    }

    /**
     * Initialize
     *
     * @return void
     */
    public function initializeFinisher()
    {
        $formData = RecipientUpdater::getFormData($this->mail);
        if (!empty($formData)) {
            $this->formId = $formData['form_id'];
            $this->subscriptionType = (int)$formData['newsletter_subscription_type'];
        }
        // @extensionScannerIgnoreLine Seems to be a false positive: getContentObject() is still correct in 9.0
        $this->contentObject = $this->configurationManager->getContentObject();
        $this->addArrayToDataArray(['uid' => $this->mail->getUid()]);
        $this->addArrayToDataArray($this->mailRepository->getVariablesWithMarkersFromMail($this->mail));
        $this->configuration = ConfigurationUtility::getAuthData();
    }

    /**
     * @param ConfigurationManagerInterface $configurationManager
     * @return void
     */
    public function injectConfigurationManager(ConfigurationManagerInterface $configurationManager)
    {
        $this->configurationManager = $configurationManager;
    }
}
