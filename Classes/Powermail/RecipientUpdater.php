<?php

namespace KDN\KdnNewsletter\Powermail;

use In2code\Powermail\Domain\Model\Field;
use In2code\Powermail\Domain\Model\Form;
use In2code\Powermail\Domain\Model\Mail;
use In2code\Powermail\Domain\Model\Page;
use In2code\Powermail\Domain\Repository\MailRepository;
use In2code\Powermail\Utility\ObjectUtility;
use KDN\KdnNewsletter\Api\Newsletter2Go;
use KDN\KdnNewsletter\Utility\ConfigurationUtility;
use KDN\KdnNewsletter\Utility\DbUtility;
use KDN\KdnNewsletter\Utility\TcaUtility;
use Symfony\Component\Console\Output\OutputInterface;
use TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface;

class RecipientUpdater
{

    /**
     * @var MailRepository
     */
    protected $mailRepository;

    /**
     * @param MailRepository $mailRepository
     * @return void
     */
    public function injectMailRepository(MailRepository $mailRepository)
    {
        $this->mailRepository = $mailRepository;
    }

    /**
     * @var OutputInterface
     */
    private $output;

    /**
     * @var PersistenceManagerInterface
     */
    protected $persistenceManager;

    /**
     * @param PersistenceManagerInterface $persistenceManager
     * @internal
     */
    public function injectPersistenceManager(PersistenceManagerInterface $persistenceManager): void
    {
        $this->persistenceManager = $persistenceManager;
    }

    /**
     * @param OutputInterface $output
     */
    public function setOutput(OutputInterface $output): void
    {
        $this->output = $output;
    }

    public function run(): int
    {
        $updateCount = 0;
        $where = 'api_mapping_state IN (1,90,91)';
        $processMails = DbUtility::fetchRows(['uid', 'api_mapping_state',], TcaUtility::TABLE_MAIL, $where);
        $configuration = ConfigurationUtility::getAuthData();
        if ($configuration && !empty($processMails) && null !== $this->mailRepository) {
            $apiService = Newsletter2Go::createInstance($configuration);
            $groupList = $apiService->getGroupList();
            $updateCount = count($processMails);
            /** @var MailRepository $mailRepository */
            $mailRepository = ObjectUtility::getObjectManager()->get(MailRepository::class);
            foreach ($processMails as $row) {
                $mailId = (int)$row['uid'];
                $mappingState = (int)$row['api_mapping_state'];
                /** @var \In2code\Powermail\Domain\Model\Mail|null $mailModel */
                $mailModel = $this->mailRepository->findByUid($mailId);
                if (null !== $mailModel) {
                    self::updateProfileData($mailRepository, $mailModel, $mappingState, $groupList, $apiService);
                }
            }
        }
        return $updateCount;
    }

    public static function updateProfileData(
        MailRepository $mailRepository,
        Mail $mailModel,
        int $mappingState,
        array $groupList,
        Newsletter2Go $apiService
    ): void
    {
        $mailData = self::processMailData($mailRepository, $mailModel);
        $newMappingState = $mappingState;
        $categories = self::getEnabledCategories($mailModel);
        $assignGroups = [];
        if (!empty($categories)) {
            foreach ($categories as $externalGroupId) {
                if (array_key_exists($externalGroupId, $groupList)) {
                    $assignGroups[$externalGroupId] = $groupList[$externalGroupId];
                }
            }
        }
        if (!empty($assignGroups) && ($formData = self::getFormData($mailModel)) && !empty($mailData['email'])) {
            $recipientModel = $apiService->getRecipientDetailsByEmail($mailData['email']);
            if (null !== $recipientModel) {
                $recGroups = $recipientModel->group_ids ?: [];
                //$recLists = $recipientModel->list_ids ?: [];
                $countAssignedGroups = 0;
                $updatedFields = [];
                $fields = ['gender', 'first_name', 'last_name', 'phone'];
                foreach ($fields as $field) {
                    if (array_key_exists($field, $mailData) && $mailData[$field] !== $recipientModel->$field) {
                        $updatedFields[$field] = $mailData[$field];
                    }
                }
                if (!empty($updatedFields) && !empty($recipientModel->list_ids)) {
                    $updateRecipientResult = $apiService->updateRecipient(
                        current($recipientModel->list_ids),
                        $recipientModel->id,
                        $recipientModel->email,
                        $updatedFields['phone'] ?: $recipientModel->phone,
                        $updatedFields['gender'] ?: $recipientModel->gender,
                        $updatedFields['first_name'] ?: $recipientModel->first_name,
                        $updatedFields['last_name'] ?: $recipientModel->last_name,
                        $recipientModel->is_unsubscribed,
                        $recipientModel->is_blacklisted
                    );
                }
                foreach ($assignGroups as $groupData) {
                    /*if (!in_array($groupData['list_id'], $recLists, false)) {
                        $setListResult = $apiService->updateRecipient(
                            $groupData['list_id'],
                            $recipientModel->id,
                            $mailData['email'],
                            $recipientModel->phone,
                            $recipientModel->gender,
                            $recipientModel->first_name,
                            $recipientModel->last_name,
                            $recipientModel->is_unsubscribed,
                            $recipientModel->is_blacklisted
                        );
                        $recLists[] = $groupData['list_id'];
                        echo '<pre>$setListResult: '.print_r($setListResult, true).'</pre>';
                    }*/
                    if (!in_array($groupData['group_id'], $recGroups, false)) {
                        $apiService->addRecipientSegment($groupData['list_id'], $groupData['group_id'], $recipientModel->id);
                        $recGroups[] = $groupData['group_id'];
                        //$segmentRecipients = $apiService->getRecipientSegment($groupData['list_id'], $groupData['group_id']);
                        ++$countAssignedGroups;
                    }
                }
                $newMappingState = $countAssignedGroups > 0 ? 2 : 3;
            } else {
                $newMappingState = 99;
            }
        }
        if ($newMappingState !== $mappingState) {
            self::updateMappingState($mailModel->getUid(), $newMappingState);
        }
    }

    public static function processMailData(MailRepository $mailRepository, Mail $mail): array
    {
        $data = array_merge(['uid' => $mail->getUid()], $mailRepository->getVariablesWithMarkersFromMail($mail));
        $email = '';
        if (!empty($data['optin_email'])) {
            $email = $data['optin_email'];
        } elseif (!empty($data['optout_email'])) {
            $email = $data['optout_email'];
        } elseif (!empty($data['email'])) {
            $email = $data['email'];
        }
        $data['email'] = $email;
        $gender = '';
        if (!empty($data['gender'])) {
            $gender = AnswerValueMapper::mapSalutationToGender($data['gender'], false);
        }
        $data['gender'] = $gender;
        return $data;
    }

    public static function updateMappingState(int $mailId, int $mappingState)
    {

        $identifier = ['uid' => $mailId];
        $bind = [
            'api_mapping_state' => $mappingState,
        ];
        $connection = DbUtility::getConnection(TcaUtility::TABLE_MAIL);
        $connection->update(TcaUtility::TABLE_MAIL, $bind, $identifier);
    }

    /**
     * Returns the mapping to the external id for the categories that are selected in the form answer
     *
     * @param Mail $mail
     * @return array<int, string>
     */
    public static function getEnabledCategories(Mail $mail): array
    {
        $categories = [];
        foreach ($mail->getAnswers() as $answer) {
            if (!method_exists($answer, 'getField') || !method_exists($answer->getField(), 'getMarker')) {
                continue;
            }
            $value = $answer->getValue();
            if ($answer->getField()->getMarker() === 'categories') {
                $categories = $value;
            }
        }
        $matchingCategories = [];
        if (!empty($categories)) {
            $where = 'LENGTH(newsletter_segment_id) > 0';
            $mappedCategories = DbUtility::fetchRows(['uid', 'title', 'newsletter_segment_id'], 'sys_category', $where);
            if (!empty($mappedCategories)) {
                foreach ($mappedCategories as $category) {
                    $title = strtolower($category['title']);
                    foreach ($categories as $compareTitle) {
                        if ($title === strtolower($compareTitle)) {
                            $matchingCategories[(int)$category['uid']] = $category['newsletter_segment_id'];
                        }
                    }
                }
            }
        }
        return $matchingCategories;
    }

    public static function getFormData($powermailModel): ?array
    {
        $form = null;
        if ($powermailModel instanceof Form) {
            $form = $powermailModel;
        } elseif ($powermailModel instanceof Mail) {
            $form = $powermailModel->getForm();
        } elseif ($powermailModel instanceof Field) {
            $page = $powermailModel->getPages();
            if ($page instanceof Page) {
                $form = $page->getForms();
            }
        }
        if (null !== $form) {
            $where = 'uid = ' . $form->getUid();
            $dbRecord = DbUtility::fetchRow(['form_id', 'newsletter_subscription_type'], TcaUtility::TABLE_FORM, $where);
            if (!empty($dbRecord['newsletter_subscription_type']) && !empty($dbRecord['form_id'])) {
                return $dbRecord;
            }
        }
        return null;
    }
}