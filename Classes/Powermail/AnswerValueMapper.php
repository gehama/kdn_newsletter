<?php
declare(strict_types=1);

namespace KDN\KdnNewsletter\Powermail;

/**
 * Class AnswerValueMapper
 */
class AnswerValueMapper
{
    public const GENDER_FEMALE = 'f';
    public const GENDER_MALE = 'm';
    public const GENDER_DIVERSE = 'v';

    /**
     * Map the given salutation to the corresponding gender (if possible)
     * @param string $salutation
     * @param bool $enableDiverse Enable usage of gender "diverse"
     * @return string
     */
    public static function mapSalutationToGender(string $salutation, $enableDiverse = true): string
    {
        switch ($salutation) {
            case 'Divers':
            case '3':
            case 'd':
                $gender = $enableDiverse ? self::GENDER_DIVERSE : '';
                break;
            case 'Frau':
            case 'Ms':
            case '2':
            case 'f':
                $gender = self::GENDER_FEMALE;
                break;
            case 'Herr':
            case 'Mr':
            case '1':
            case 'm':
                $gender = self::GENDER_MALE;
                break;
            default:
                $gender = '';
                break;
        }
        return $gender;
    }
}
