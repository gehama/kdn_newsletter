<?php
declare(strict_types=1);

namespace KDN\KdnNewsletter\Utility;

use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use TYPO3\CMS\Core\Database\Query\Restriction\EndTimeRestriction;
use TYPO3\CMS\Core\Database\Query\Restriction\HiddenRestriction;
use TYPO3\CMS\Core\Database\Query\Restriction\StartTimeRestriction;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class DbUtility
 */
class DbUtility
{

    public static function getConnection(string $table): Connection
    {
        return GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable($table);
    }

    public static function getQueryBuilder(string $table): QueryBuilder
    {
        return self::getConnection($table)->createQueryBuilder();
    }

    /**
     * Load record from database
     *
     * @param array|string $select SELECT string or field list
     * @param string $from FROM string
     * @param string $where WHERE string
     *
     * @return array|false Table row array; false if empty
     */
    public static function fetchRow($select, $from, $where)
    {
        $queryBuilder = self::createQueryBuilder($select, $from, $where);
        $statement = $queryBuilder->execute();
        return $statement->fetch();
    }

    /**
     * Load records from database
     *
     * @param array|string $select SELECT string or field list
     * @param string $from FROM string
     * @param string $where WHERE string
     *
     * @return array|false Table row array; false if empty
     */
    public static function fetchRows($select, $from, $where)
    {
        $queryBuilder = self::createQueryBuilder($select, $from, $where);
        $statement = $queryBuilder->execute();
        return $statement->fetchAll();
    }

    /**
     * Creates a QueryBuilder instance with the given query parameters
     *
     * @param array|string $select SELECT string or field list
     * @param string $from FROM string
     * @param string $where WHERE string
     *
     * @return QueryBuilder $queryBuilder
     */
    private static function createQueryBuilder($select, $from, $where): QueryBuilder
    {
        /** @var \TYPO3\CMS\Core\Database\Query\QueryBuilder $queryBuilder */
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable($from);
        $queryBuilder->resetRestrictions();
        $restrictions = $queryBuilder->getRestrictions();
        $restrictions->removeByType(HiddenRestriction::class);
        $restrictions->removeByType(StartTimeRestriction::class);
        $restrictions->removeByType(EndTimeRestriction::class);
        if (!is_iterable($select)) {
            $select = explode(', ', $select);
        }
        $queryBuilder
            ->select(...$select)
            ->from($from)
            ->where($where);
        return $queryBuilder;
    }

    /**
     * Load record from database
     *
     * @param string $table
     * @param array $identifiers
     * @param array $orderBy
     * @param string[] $extraConditions
     * @return array|null Table row array; null if empty
     */
    public static function fetchRowByFields($table, $identifiers, $orderBy = [], $extraConditions = []): ?array
    {
        $queryBuilder = self::getQueryBuilder($table);
        $queryBuilder
            ->select('*')
            ->from($table)
            ->setMaxResults(1);
        foreach ($identifiers as $field => $value) {
            $type = $field === 'uid' && is_int($value) ? \PDO::PARAM_INT : \PDO::PARAM_STR;
            $expr = $queryBuilder->expr()->eq($field, $queryBuilder->createNamedParameter($value, $type));
            $queryBuilder->andWhere($expr);
        }
        foreach ($extraConditions as $condition) {
            $queryBuilder->andWhere($condition);
        }
        foreach ($orderBy as $field => $order) {
            $queryBuilder->addOrderBy($field, $order);
        }
        $row = $queryBuilder->execute()->fetch();
        if (empty($row)) {
            return null;
        }
        return $row;
    }
}
