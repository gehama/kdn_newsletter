<?php
declare(strict_types=1);

namespace KDN\KdnNewsletter\Utility;

/**
 * Class ConfigurationUtility
 */
class ConfigurationUtility
{
    public const EXT_KEY = 'kdn_newsletter';

    /**
     * @param string $key Configuration key
     *
     * @return mixed|null
     */
    public static function getExtConfValue($key)
    {
        $confArr = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Configuration\ExtensionConfiguration::class)->get(self::EXT_KEY);
        if (!empty($confArr[$key])) {
            return $confArr[$key];
        }
        return null;
    }

    public static function getAuthData(): ?array
    {
        $keys = ['authKey', 'authEmail', 'authPassword'];
        $authData = [];
        foreach ($keys as $key) {
            $value = self::getExtConfValue($key);
            if (empty($value)) {
                return null;
            }
            $authData[$key] = $value;
        }
        $authData['authProxy'] = self::getExtConfValue('authProxy');

        return $authData;
    }
}
