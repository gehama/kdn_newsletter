<?php
declare(strict_types=1);

namespace KDN\KdnNewsletter\Utility;

/**
 * Class TcaUtility
 */
class TcaUtility
{
    public const TABLE_FORM = 'tx_powermail_domain_model_form';
    public const TABLE_MAIL = 'tx_powermail_domain_model_mail';
}