<?php

namespace KDN\KdnNewsletter\Api;

use KDN\KdnNewsletter\Utility\ConfigurationUtility;
use TYPO3\CMS\Core\Cache\CacheManager;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class NewsletterItemProcHelper
{

    /**
     * User function for Newsletter2Go form codes
     *
     * @param array $PA the array with additional configuration options.
     */
    public function addAddressBookItems(&$PA)
    {
        $newsletterSettings = ConfigurationUtility::getAuthData();
        if ($newsletterSettings) {
            $apiService = Newsletter2Go::createInstance($newsletterSettings);
            $result = $apiService->getLists();
            if ($result instanceof \stdClass && $result->value) {
                foreach ($result->value as $list) {
                    $item = [
                        htmlspecialchars($list->name),
                        $list->id
                    ];
                    $PA['items'][] = $item;
                }
            }
        }

    }

    /**
     * User function for Newsletter2Go list groups
     *
     * @param array $PA the array with additional configuration options.
     */
    public function addListGroupItems(&$PA)
    {
        $newsletterSettings = ConfigurationUtility::getAuthData();
        if ($newsletterSettings) {
            /** @var \TYPO3\CMS\Core\Cache\Frontend\FrontendInterface $cache */
            $cache = GeneralUtility::makeInstance(CacheManager::class)->getCache('kdn_newsletter_group');
            $cacheIdentifier = sha1('kdn_newsletter_groups');

            $entry = $cache->get($cacheIdentifier);
            if (!$entry || $entry['tstamp'] < time() - 3600) {
                $apiService = Newsletter2Go::createInstance($newsletterSettings);
                $entry = [
                    'tstamp' => time(),
                    'items' => $apiService->getGroupList(),
                ];
                $cache->set($cacheIdentifier, $entry);
            }
            foreach ($entry['items'] as $item) {
                $item = [
                    htmlspecialchars($item['list_name'] . ': ' . $item['group_name']),
                    $item['group_id']
                ];
                $PA['items'][] = $item;
            }

        }

    }
}