<?php

namespace KDN\KdnNewsletter\Service;


use TYPO3\CMS\Core\Authentication\BackendUserAuthentication;
use TYPO3\CMS\Core\Utility\GeneralUtility;


class ApiHelper
{
    /**
     * @param bool $override Optionally override existing backend user
     * @return BackendUserAuthentication
     */
    public static function getBackendUser($override = false)
    {

        if ($override || !isset($GLOBALS['BE_USER']) || !($GLOBALS['BE_USER'] instanceof BackendUserAuthentication)) {
            // create global objects
            if (!$GLOBALS['LANG']) {
                $GLOBALS['LANG'] = GeneralUtility::makeInstance(\TYPO3\CMS\Core\Localization\LanguageService::class);
            }
            // required in \TYPO3\CMS\Core\DataHandling\DataHandler::isTableAllowedForThisPage
            if (!$GLOBALS['PAGES_TYPES']['default']['allowedTables']) {
                $GLOBALS['PAGES_TYPES']['default']['allowedTables'] = '*';
            }

            /** @var BackendUserAuthentication $backendUser */
            $backendUser = GeneralUtility::makeInstance(BackendUserAuthentication::class);
            $backendUser->dontSetCookie = TRUE;
            $backendUser->user['uid'] = 0;
            $backendUser->user['username'] = 'feManager_virtualUser';
            $backendUser->user['admin'] = 1;
            $backendUser->workspace = 0;
            $backendUser->user['uc']['recursiveDelete'] = FALSE;
            $backendUser->workspaceRec['live_edit'] = TRUE;
            $GLOBALS['BE_USER'] = $backendUser;
        }
        return $GLOBALS['BE_USER'];
    }
}