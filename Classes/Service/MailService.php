<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2021 Gert Hammes
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace KDN\KdnNewsletter\Service;

use Psr\Log\LoggerInterface;
use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Log\LogLevel;
use TYPO3\CMS\Core\Log\LogManager;
use TYPO3\CMS\Core\Mail\MailMessage;
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController;

/**
 * Abstract away sending emails into a service:
 */
class MailService extends AbstractService implements SingletonInterface
{
    /**
     * Send profile edit request email with link to edit form
     *
     * @param string $email
     * @param string $hash
     * @param array $settings
     * @param int|null $pid
     */
    public function sendProfileEditRequestEmail(string $email, string $hash, array $settings, $pid = null): void
    {
        $arguments = ['hash' => $hash];
        if (!$pid) {
            $pid = (int) $this->getTypoScriptFrontendController()->id;
        }
        $cancelUrl = $this->createEventActionLink($pid, 'edit', $arguments);
        $mailSettings = $settings['mail'];
        $subject = $settings['confirmationEmailSubject'] ?: 'KDN: Ihre Newsletter-Einstellungen';
        $cancelLink = '<a href="'.$cancelUrl.'">'.$cancelUrl.'</a>';
        $markers['###CANCEL_LINK###'] = $cancelLink;
        $markers['###CANCEL_URL###'] = $cancelUrl;
        $messageBodyTemplate = $settings['confirmationEmailContent'] ?: '<p>Hallo,</p>
<p>vielen Dank für Ihre Anfrage.<br>Bitte klicken Sie auf den folgenden Link, um Ihre Newsletter-Einstellungen zu bearbeiten:<br>
###CANCEL_LINK###</p>
<p>Mit freundlichen Grüßen<br>
Ihr KDN-Presse-Team</p>';
        $messageHtml = str_replace(array_keys($markers), $markers, $messageBodyTemplate);
        $messagePlain = trim(strip_tags($messageHtml));
        if (strpos($messageHtml, '<body') === false) {
            $messageHtml = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                    <html xmlns="http://www.w3.org/1999/xhtml" lang="de">
                     <head>
                      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                      <title>' . $subject . '</title>
                      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                    </head>
                    <body style="margin: 0; padding: 0;">' . $messageHtml . '</body>
                    </html>';
        }
        $this->send($email, $subject, $messageHtml, $messagePlain, $mailSettings);
    }

    /**
     * @return TypoScriptFrontendController
     * @SuppressWarnings(PHPMD.Superglobals)
     */
    protected function getTypoScriptFrontendController()
    {
        return $GLOBALS['TSFE'];
    }

    private function createEventActionLink($pageUid, $action, $arguments)
    {
        $extensionName = 'kdnnewsletter';
        $pluginName = 'subscription';
        $controller = 'Subscription';
        $uriBuilder = $this->getUriBuilder();
        if (null !== $uriBuilder) {
            $url = $uriBuilder
                ->reset()
                ->setTargetPageUid($pageUid)
                //->setTargetPageType(0)
                //->setNoCache(false)
                ->setUseCacheHash(true)
                ->setLinkAccessRestrictedPages(true)
                //->setArguments($additionalParams)
                ->setCreateAbsoluteUri(true)
                //->setAddQueryString($addQueryString)
                ->uriFor($action, $arguments, $controller, $extensionName, $pluginName);
            if (strpos($url, 'http') !== 0) {
                $url = 'https://www.kdn.de/' . ltrim($url, '/');
            }
            return $url;
        }
        return null;
    }

    /**
     * Send a mail via html mailer
     *
     * @param string $recipient
     * @param string $subject
     * @param string $messageHtml
     * @param string $messagePlain
     * @param array $mailSettings
     * @param array $attachments
     * @return bool
     */
    private function send(
        $recipient,
        $subject,
        $messageHtml,
        $messagePlain,
        $mailSettings,
        array $attachments = array()
    ): bool
    {
        $status = TRUE;
        /** @var MailMessage $mailer */
        $mailer = GeneralUtility::makeInstance(MailMessage::class);
        try {
            $mailer->setFrom(array($mailSettings['fromEmail'] => $mailSettings['fromName']));
            if (!empty($mailSettings['forceSenderEmail'])) {
                $mailer->setSender($mailSettings['forceSenderEmail']);
                $mailer->setReplyTo($mailSettings['fromEmail']);
            } else {
                $mailer->setSender($mailSettings['fromEmail'], $mailSettings['fromName']);
            }
            if (!empty($mailSettings['replyTo'])) {
                $mailer->setReplyTo($mailSettings['replyTo']);
            }
            if (!empty($mailSettings['returnPath'])) {
                $mailer->setReturnPath($mailSettings['returnPath']);
            }
            $mailer->setTo($recipient)
                ->setSubject($subject);
            if (!empty($mailSettings['bccEmail'])) {
                $mailer->setBcc($mailSettings['bccEmail']);
            }
            if (!empty($mailSettings['ccEmail'])) {
                $mailer->setCc($mailSettings['ccEmail']);
            }
            $this->setMailParts($mailer, $messageHtml, $messagePlain, $attachments, $mailSettings);
            $mailer->send();
        } catch (\Exception $e) {
            $this->getLogger()->log(LogLevel::ERROR, $e->getMessage(), ['extension' => 'kdn_events']);
            $status = !$status;
        }

        return $status;
    }

    protected function isGteVersion10(): bool
    {
        $cmsVersion = (string)\TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Information\Typo3Version::class);
        return \TYPO3\CMS\Core\Utility\VersionNumberUtility::convertVersionNumberToInteger($cmsVersion) > 10000000;
    }

    protected function setMailParts(
        MailMessage $mailer,
        string $messageHtml,
        string $messagePlain,
        array $attachments,
        array $mailSettings
    )
    {
        if ($this->isGteVersion10()) {
            if (!empty($messageHtml)) {
                if ($mailSettings['embedImages']) {
                    $messageHtml = $this->embedImages($mailer, $messageHtml);
                }
                $mailer->html($messageHtml);
                $mailer->text($messagePlain);
            } else {
                $mailer->text($messagePlain);
            }
            if (!empty($attachments)) {
                foreach ($attachments as $absPath) {
                    /** @noinspection PhpUndefinedMethodInspection */
                    $mailer->attachFromPath($absPath);
                }
            }
        } else {
            if (!empty($messageHtml)) {
                if ($mailSettings['embedImages']) {
                    $messageHtml = $this->embedImages($mailer, $messageHtml);
                }
                $mailer->setBody($messageHtml, 'text/html');
                $mailer->addPart($messagePlain, 'text/plain');
            } else {
                $mailer->setBody($messagePlain, 'text/plain');
            }
            if (!empty($attachments)) {
                foreach ($attachments as $absPath) {
                    $mailer->attach(\Swift_Attachment::fromPath($absPath));
                }
            }
        }
    }

    protected function getLogger(): LoggerInterface
    {
        /** @var LogManager $logManager */
        $logManager = GeneralUtility::makeInstance(LogManager::class);
        return $logManager->getLogger(__CLASS__);
    }

    /**
     * Embed image in mail instead of only sending the link to the images.
     * This is necessary when the email contains temporary images
     *
     * @param MailMessage $mailer
     * @param string $htmlPart
     * @return string
     */
    private function embedImages(MailMessage $mailer, $htmlPart): string
    {
        $matches = array();
        //Search for all src attributes
        $pattern = '/src=[\'"]?([^\'" >]+)[\'" >]/';
        preg_match_all($pattern, $htmlPart, $matches);
        if (!empty($matches[1])) {
            $imgSrcArr = $matches[1];
            $baseUrl = isset($GLOBALS['TSFE']) && $GLOBALS['TSFE']->tmpl->setup['config.']['baseURL']
                ? $GLOBALS['TSFE']->tmpl->setup['config.']['baseURL']
                : GeneralUtility::getIndpEnv('TYPO3_SITE_URL');
            $relDirs = array('fileadmin/', 'uploads/', 'typo3temp/', 'typo3temp/');
            foreach ($imgSrcArr as $imgSrc) {
                //if image is used more than once, make sure it is embedded only once
                if (strpos($htmlPart, $imgSrc) !== false) {
                    $imgRelPath = str_replace($baseUrl, '', $imgSrc);
                    foreach ($relDirs as $subDir) {
                        $relDirStartPos = strpos($imgRelPath, $subDir);
                        if ($relDirStartPos > 0) {
                            $imgRelPath = substr($imgRelPath, $relDirStartPos);
                            break;
                        }
                    }
                    $absolutePath = Environment::getPublicPath() . DIRECTORY_SEPARATOR
                        . ltrim($imgRelPath, DIRECTORY_SEPARATOR);

                    if ($this->isGteVersion10()) {
                        /** @noinspection PhpUndefinedMethodInspection */
                        $cid = $mailer->attachFromPath($absolutePath);
                    } else {
                        $cid = $mailer->embed(\Swift_Image::fromPath($absolutePath));
                    }
                    $htmlPart = str_replace($imgSrc, $cid, $htmlPart);
                }
            }
        }
        return $htmlPart;
    }
}
