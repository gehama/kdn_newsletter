<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Gert Hammes
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace KDN\KdnNewsletter\Service;

/**
 * Abstract export service
 */
abstract class AbstractExportService extends AbstractService
{
    protected $fields = [
        'last_name',// A
        'first_name',// B
        'email',//C
        'title',//D
        'gender',// E
        'company',// F
    ];

    const FONT_NAME = 'Arial';
    const FONT_SIZE = 10;
    const MAX_COL_WIDTH = 25;

    abstract public function create($sheetName, $rows);
    abstract public function sendFile($fileBaseName, $fileType);

    protected function getRowFieldVal($row, $field)
    {
        $getter = 'get' . str_replace(' ', '', ucwords(str_replace('_', ' ', $field)));
        $fieldVal = $getter;
        if (method_exists($row, $getter)) {
            $fieldVal = $row->$getter();
            if (is_object($fieldVal)) {
                if ($fieldVal instanceof \DateTime) {
                    $fieldVal = $fieldVal->format('d.m.Y H:i');
                } elseif (method_exists($fieldVal, 'getName')) {
                    $fieldVal = $fieldVal->getName();
                } elseif (method_exists($fieldVal, 'getTitle')) {
                    $fieldVal = $fieldVal->getTitle();
                }
            } elseif ($field == 'gender') {
                switch ($fieldVal) {
                    case 'm':
                        $fieldVal = 'Herr';
                        break;
                    case 'f':
                        $fieldVal = 'Frau';
                        break;
                    case 'd':
                        $fieldVal = 'Divers';
                        break;
                }

            }
        }
        return $fieldVal;
    }

    /**
     * Removes all special characters from a given file name.
     * E.g. Useful for auto-generated file names or renaming files
     *
     * @param string $fileName Current filename
     *
     * @return string New clean file name
     */
    protected function filterFileName($fileName)
    {
        $dir = dirname($fileName);
        if (strlen($dir) > 1) {
            $dir .= '/';
        } else {
            $dir = '';
        }
        $basename = basename($fileName);
        $replace = array(
            " " => '_',
            "%20" => "_",
            'ß' => 'ss',
            'ü' => 'ue',
            'Ü' => 'Ue',
            'ä' => 'ae',
            'Ä' => 'Ae',
            'ö' => 'oe',
            'Ö' => 'Oe',
        );
        $search = array_keys($replace);
        // clean up file name
        $newFileName = str_replace($search, $replace, $basename);
        $newFileName = $dir
            . preg_replace("/[^0-9a-zA-Z\-_\.]+/", "_", $newFileName);
        $newFileName = trim(str_replace('__', '_', $newFileName), '_');
        return $newFileName;
    }
}
