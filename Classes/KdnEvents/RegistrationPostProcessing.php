<?php


namespace KDN\KdnNewsletter\KdnEvents;


use KDN\KdnEvents\Controller\EventController;
use KDN\KdnEvents\Domain\Model\Registration;
use KDN\KdnNewsletter\Api\Newsletter2Go;
use KDN\KdnNewsletter\Powermail\OptInConfirm;
use KDN\KdnNewsletter\Utility\ConfigurationUtility;
use TYPO3\CMS\Extbase\Domain\Model\Category;

class RegistrationPostProcessing
{
    /**
     * @param Registration $registration
     * @param array $settings
     * @param EventController $originalService
     * @throws \Exception
     */
    public function postProcessRegistration(Registration $registration, array $settings, EventController $originalService): void
    {
        if ($registration->isNewsletter()) {
            $this->addNewsletterSubscription($registration);
            $this->addNewsletter2GoSubscription($registration, $settings);
        }
    }

    /**
     * Add local newsletter subscription based on selected event categories
     * @param Registration $registration
     */
    private function addNewsletterSubscription(Registration $registration): void
    {
        $event = $registration->getEvent();
        if (null !== $event && ($categories = $event->getEventCategories()) && count($categories) > 0) {
            $categoryIdList = [];
            // Check for categories on configured storage page
            $storagePid = (int) ConfigurationUtility::getExtConfValue('storagePid');
            /** @var Category $category */
            foreach ($categories as $category) {
                if ($category->getPid() === $storagePid) {
                    $categoryIdList[] = $category->getUid();
                }
            }
            // Only add subscription if at least one category was added
            if (!empty($categoryIdList)) {
                $formValues = [
                    'email' => $registration->getEmail(),
                    'gender' => $registration->getSalutation(),
                    'title' => $registration->getTitle(),
                    'first_name' => $registration->getFirstName(),
                    'last_name' => $registration->getLastName(),
                    'company' => $registration->getOrganisation(),
                    'address' => $registration->getStreet(),
                    'zip' => $registration->getZipcode(),
                    'city' => $registration->getTown(),
                    'telephone' => $registration->getPhone(),
                    'categories' => $categoryIdList,
                ];
                OptInConfirm::saveRecipient($formValues);
            }
        }
    }

    private function addNewsletter2GoSubscription(Registration $registration, array $settings)
    {
        if (($newsletterSettings = $settings['newsletter'])
            && ($subscriptionCode = $newsletterSettings['newsletter2goCode'])
            && ($authKey = $newsletterSettings['authKey'])
            && ($authEmail = $newsletterSettings['authEmail'])
            && $authPassword = $newsletterSettings['authPassword']) {
            /** @var Newsletter2Go $apiService */
            $proxy = $newsletterSettings['proxy'] ?? null;
            $apiService = new Newsletter2Go($authKey, $authEmail, $authPassword, $proxy);
            $mapSalutation = '';
            if ($registration->getSalutation() === Registration::SALUTATION_FEMALE) {
                $mapSalutation = 'f';
            } elseif ($registration->getSalutation() === Registration::SALUTATION_MALE) {
                $mapSalutation = 'm';
            }
            $apiService->addRecipientViaForm(
                $subscriptionCode,
                //$email, $first_name, $last_name, $gender
                $registration->getEmail(),
                $registration->getFirstName(),
                $registration->getLastName(),
                $mapSalutation
            );
        }
    }
}