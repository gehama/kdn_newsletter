<?php

namespace KDN\KdnNewsletter\Command;

/**
 * This file is part of the "kdn_newsletter" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use KDN\KdnNewsletter\Powermail\RecipientUpdater;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use TYPO3\CMS\Core\Core\Bootstrap;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;

/**
 * Update recipient data in the api
 *
 */
class UpdateApiRecipientsCommand extends Command
{

    /**
     * Configure the command by defining the name, options and arguments
     */
    protected function configure()
    {
        $this->setDescription('Update recipient data in the api')
            ->setHelp('Used for updating selected groups in the api');
    }

    /**
     * Executes the command for updating the related products
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $io->title($this->getDescription());
        $startTime = microtime(true);
        Bootstrap::initializeBackendAuthentication();

        $objectManager = GeneralUtility::makeInstance(ObjectManager::class);
        /** @var RecipientUpdater $updater */
        $updater = $objectManager->get(RecipientUpdater::class);
        $updater->setOutput($io);
        $updateCount = $updater->run();
        $duration = round(microtime(true) - $startTime, 3);
        $output->writeln(sprintf('Finished processing %d mails in %s seconds', $updateCount, $duration));
    }
}
